[![License: MIT](https://img.shields.io/badge/License-MIT-blue?style=flat-square)](https://opensource.org/licenses/MIT) [![Donate: Ko-Fi](https://img.shields.io/badge/Donate-Ko--Fi-%23ff5f5f?style=flat-square&logo=kofi)](https://ko-fi.com/elvispereira)

NameForge
=========

NameForge is a machine learning-assisted random name generator for Foundry VTT. It allows you to generate names during the creation of new actors, in the actor sheets and more!

Preview:

![Video Preview](img/preview.webm)

The module provides some default generators for you, but you can also create your own, all you have to do is click the `Train` button in the actors list sidebar and past a list of comma separated names.

---

## Installation

This module is available in the Add-on Modules list under the name `NameForge`.

### Installation by Manifest

In the option `Add-On Modules` click on `Install Module` and paste the following link in the `Manifest URL` field:

`https://gitlab.com/elvispereira/nameforge/-/raw/main/module.json`

### Manual Installation (Not Recommended)

If the above options do not work visit the [releases](https://gitlab.com/elvispereira/nameforge/-/releases) page, find the version you want to use and download the `module.zip` file, after that extract the contents into the `Data/modules/nameforge` folder.

---

## Contact

You can file a new [issue](https://gitlab.com/elvispereira/nameforge/-/issues) here or reach out to me in Discord Bellenus#5269

---

## License

The code of this module is released under the [MIT license](LICENSE), the trained neural networks inside the `models` folder and the lists of names inside the `sources` folder are released under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

---

## Special thanks to

Orion#8710 for providing most of the name lists.

Brother Sharp#6921 for helping with the Japanese names.

---

## Modification test

spossos1